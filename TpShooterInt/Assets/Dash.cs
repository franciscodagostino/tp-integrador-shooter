﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dash : MonoBehaviour
{

    private float cooldown = 8;
    private float nextDash = 0;
    public float dashSpeed = 60f;
    public Camera camFps;
    Rigidbody rb;

    private void Start()
    {
       
        rb = GetComponent<Rigidbody>();

    }

    private void Update()
    {

        if (Time.time > nextDash)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                nextDash = Time.time + cooldown;
                rb.AddForce(camFps.transform.forward * dashSpeed, ForceMode.Impulse);

            }
        }
    }
}
