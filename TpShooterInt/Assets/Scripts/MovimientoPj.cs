﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoPj : MonoBehaviour
{
    private Rigidbody rb;
    public float rapidezDesplazamiento;
    public LayerMask capaPiso;
    public float magnitudSalto;
    public CapsuleCollider col;
    public Camera camaraPrimeraPersona;
   


    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Cursor.lockState = CursorLockMode.Locked;
        
    }


    private void Update()
    {
        if (EstaEnPiso() && Input.GetKeyDown(KeyCode.Space))

        {
             rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
        }
      
    }


    private bool EstaEnPiso()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x,
        col.bounds.min.y, col.bounds.center.z), col.radius * .9f, capaPiso);
    }

   

    private void FixedUpdate()
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);


        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }

        
       
  

    }



}

